from django.apps import AppConfig


class FistSchoolConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'modules.courses.fist_school'
