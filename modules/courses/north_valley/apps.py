from django.apps import AppConfig


class NorthValleyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'modules.courses.north_valley'
