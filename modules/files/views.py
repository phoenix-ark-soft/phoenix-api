from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated


class FileLoaderView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request: Request):
        files = request.data
        return Response("")