# Generated by Django 4.2.2 on 2023-09-17 12:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0004_remove_usermenu_children_usermenu_parent_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='usermenu',
            name='order_priority',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
