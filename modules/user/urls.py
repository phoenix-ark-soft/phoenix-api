from django.urls import path
from rest_framework import routers

from .views.ria.logout import Logout
from .views.ria.registration import Registration
from .views.ria.authorization import Authorization
from .views.ria.get_new_tokens import NewTokenPair
from .views.ria.confirm_registration import ConfirmRegistration
from .views.ria.full_logout import LogoutAll
from .views.ria.confirm_repair_account import ForgotPasswordConfirm
from .views.user.get_user import UserViewSet
from .views.user.change_email import UserEmailChange
from .views.user.change_avatar import UserAvatarChange
from .views.user.change_password import UserPasswordChange
from .views.user.change_private_data import UserPrivateDataChange
from .views.user.get_menu import UserMenuView

router = routers.DefaultRouter()
router.register("users", UserViewSet)
urlpatterns = [
    path("users/change/password/", UserPasswordChange.as_view()),
    path("users/change/avatar/", UserAvatarChange.as_view()),
    # path("users/change/username/", UserUsernameChange.as_view()),
    path("users/change/private-data/", UserPrivateDataChange.as_view()),
    path("users/change/email/", UserEmailChange.as_view()),
    path("register/", Registration.as_view()),
    path("authorize/", Authorization.as_view()),
    path("logout/", Logout.as_view()),
    path("token-pair/", NewTokenPair.as_view()),
    path("confirm/", ConfirmRegistration.as_view()),
    path("forgot-password/", ForgotPasswordConfirm.as_view()),
    path("confirm-password/", ForgotPasswordConfirm.as_view()),
    path("get-menu/", UserMenuView.as_view())
] + router.urls
