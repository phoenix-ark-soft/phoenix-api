from django.contrib.auth.hashers import check_password
from rest_framework import permissions, status
from rest_framework.generics import CreateAPIView, DestroyAPIView
from rest_framework.response import Response

from core.utils.ria import get_refresh_dict

from middlewares.auth import AuthenticationSystem
from ...models import AccessTokens


class LogoutAll(DestroyAPIView):
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [AuthenticationSystem]

    def delete(self, request, *args, **kwargs):
        AccessTokens.objects.filter(user_id=request.user.id).delete()
        response = Response(status=status.HTTP_204_NO_CONTENT)
        response.set_cookie(**get_refresh_dict())
        return response
