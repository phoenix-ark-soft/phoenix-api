from django.contrib.auth import get_user_model
from rest_framework import permissions, status
from rest_framework.generics import CreateAPIView
from rest_framework.request import Request
from rest_framework.response import Response

from core.exception import ClientException, ServerException

from ...serializers import ConfirmRegistrationSerializer

User = get_user_model()


class ConfirmRegistration(CreateAPIView):
    permission_classes = [permissions.AllowAny]
    serializer_class = ConfirmRegistrationSerializer
    authentication_classes = []

    def post(self, request: Request, *args, **kwargs):
        token = request.data.get("token")
        candidates = User.objects.filter(token_email=token)
        if len(candidates) == 1:
            user = candidates[0]
            serializer = self.serializer_class(user, data=request.data, partial=True)
            if not serializer.is_valid():
                raise ServerException()
            serializer.save()
            return Response(status=204)
        raise ClientException("Ошибка подтверждения почты", status.HTTP_400_BAD_REQUEST)