from rest_framework import permissions, status
from rest_framework.generics import CreateAPIView
from rest_framework.request import Request
from rest_framework.response import Response

from core.exception import ClientException

from ...serializers import RegistrationSerializer


class Registration(CreateAPIView):
    permission_classes = [permissions.AllowAny]
    serializer_class = RegistrationSerializer
    authentication_classes = []

    def post(self, request: Request, *args, **kwargs):
        data = self.serializer_class(data=request.data)
        if not data.is_valid():
            raise ClientException(data.errors)
        data.save()
        return Response(status=status.HTTP_201_CREATED)

