from django.contrib.auth import get_user_model
from rest_framework import permissions
from rest_framework.generics import CreateAPIView
from rest_framework.request import Request
from rest_framework.response import Response

from core.exception import ClientException, ServerException

from ...serializers import PasswordChangeSerializer

User = get_user_model()


class RepairAccount(CreateAPIView):
    authentication_classes = []
    permission_classes = [permissions.AllowAny]
    serializer_class = PasswordChangeSerializer

    def post(self, request: Request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            raise ClientException(serializer.errors)
        candidates = User.objects.filter(email=serializer.validated_data.get("email"))
        if len(candidates) == 1:
            serializer.instance = candidates[0]
            serializer.save()
            return Response(status=200)
        raise ServerException()

