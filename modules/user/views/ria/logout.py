from rest_framework import permissions, status
from rest_framework.generics import DestroyAPIView
from rest_framework.request import Request
from rest_framework.response import Response

from core.utils.ria import get_refresh_dict

from middlewares.auth import AuthenticationSystem
from ...models import AccessTokens


class Logout(DestroyAPIView):
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [AuthenticationSystem]

    def delete(self, request: Request, *args, **kwargs):
        AccessTokens.objects.get(token=request.auth.token).delete()
        response = Response(status=status.HTTP_204_NO_CONTENT)
        response.set_cookie(**get_refresh_dict())
        return response