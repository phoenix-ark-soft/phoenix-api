from django.contrib.auth import get_user_model
from rest_framework import permissions, status
from rest_framework.generics import CreateAPIView
from rest_framework.request import Request
from rest_framework.response import Response

from core.exception import ClientException, ServerException

from ...serializers import ConfirmPasswordChangeSerializer


User = get_user_model()


class ForgotPasswordConfirm(CreateAPIView):
    authentication_classes = []
    permission_classes = [permissions.AllowAny]
    serializer_class = ConfirmPasswordChangeSerializer

    def post(self, request: Request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            raise ClientException(serializer.errors)
        candidates = User.objects.filter(token_password_change=serializer.validated_data.get("token"))
        if len(candidates) == 1:
            serializer.instance = candidates[0]
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        raise ServerException()

