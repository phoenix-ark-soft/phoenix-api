from rest_framework import permissions, status
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from core.exception import ClientException
from core.security.jwt import generate_jwt_pair, decode_jwt
from core.utils.ria import get_refresh_dict

from ...models import AccessTokens, RefreshTokens


class NewTokenPair(APIView):
    permission_classes = [permissions.AllowAny]
    authentication_classes = []

    def post(self, request: Request, *args, **kwargs):
        token = request.COOKIES.get("refresh")
        if token is None or token == '':
            raise ClientException("Необходимо перезайти", status.HTTP_400_BAD_REQUEST)
        decoded_token = decode_jwt(token)
        if not decoded_token.is_valid:
            raise ClientException("Необходимо перезайти", status.HTTP_401_UNAUTHORIZED)
        refresh_model = RefreshTokens.objects.get(token=decoded_token.token)
        new_pair = generate_jwt_pair(refresh_model.user)  # User.objects.get(id=decoded_token.payload.get("id"))
        AccessTokens.objects.get(token=refresh_model.access.token).delete()
        response = Response({"access": new_pair.access.token}, status=status.HTTP_200_OK)
        response.set_cookie(**get_refresh_dict(new_pair.refresh.token))
        return response