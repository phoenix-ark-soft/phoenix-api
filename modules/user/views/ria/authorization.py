from django.contrib.auth.hashers import check_password
from rest_framework import permissions, status
from rest_framework.generics import CreateAPIView, DestroyAPIView
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from core.exception import ClientException, ServerException
from core.security.jwt import generate_jwt_pair, decode_jwt
from core.utils.ria import get_refresh_dict

from ...models import UserMainModel as User, AccessTokens, RefreshTokens
from ...serializers import AuthorizationSerializer, RegistrationSerializer, AuthRespSerializer, \
    ConfirmRegistrationSerializer, ConfirmPasswordChangeSerializer, PasswordChangeSerializer


class Authorization(CreateAPIView):
    permission_classes = [permissions.AllowAny]
    serializer_class = AuthorizationSerializer
    authentication_classes = []

    def post(self, request, *args, **kwargs):
        data = request.data
        try:
            candidate = User.objects.get(username=data.get("username"))
        except Exception as e:
            raise ClientException("Неверный логин или пароль", status.HTTP_400_BAD_REQUEST)
        if not candidate.is_confirmed_email:
            raise ClientException("Не подтверждена почта", status.HTTP_400_BAD_REQUEST)
        if check_password(data.get("password"), candidate.password):
            pair = generate_jwt_pair(candidate)
            response_data = {
                "access": pair.access.token,
                "user": candidate
            }
            response_cookie = get_refresh_dict(pair.refresh.token)
            response = Response(AuthRespSerializer(response_data).data, status=status.HTTP_200_OK)
            response.set_cookie(**response_cookie)
            return response
        raise ClientException("Неверный логин или пароль", status.HTTP_400_BAD_REQUEST)