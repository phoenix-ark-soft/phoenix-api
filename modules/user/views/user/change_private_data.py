from django.contrib.auth import get_user_model
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from core.constants import TAG_USER

User = get_user_model()


class UserPrivateDataChange(APIView):
    permission_classes = [IsAuthenticated]
    swagger_tags = [TAG_USER]

    def patch(self, request: Request, *args, **kwargs):
        fields = ["first_name", "last_name", "skype", "discord", "phone", "patronymic"]
        try:
            changed_data = {}
            for item in fields:
                if item in request.data:
                    value = request.data.get(item)
                    setattr(request.user, item, value)
                    changed_data[item] = value
            request.user.save()
            return Response(changed_data, status=200)
        except Exception as e:
            print(f"{request.method} {request.path}:", str(self), str(e))
            return Response(str(e), status=400)