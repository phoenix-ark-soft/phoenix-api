from django.contrib.auth import get_user_model
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from core.utils.ria import request_email_confirmation, check_password_by_request
from core.constants import TAG_USER

User = get_user_model()


class UserEmailChange(APIView):
    permission_classes = [IsAuthenticated]
    swagger_tags = [TAG_USER]

    def patch(self, request: Request, *args, **kwargs):
        if not check_password_by_request(request):
            return Response("Пароли не совпадают", status=403)
        email = request.data.get("email")
        if email:
            request_email_confirmation(request.user, email)
            return Response(status=200)
        else:
            return Response("Поле email не было предоставлено", status=400)
