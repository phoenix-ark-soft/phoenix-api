import json

from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from core.constants import TAG_USER
from ...models import UserMenu
from ...serializers import UserMenuSerializer


class UserMenuView(APIView):
    permission_classes = [IsAuthenticated]
    swagger_tags = [TAG_USER]

    def get(self, request: Request, *args, **kwargs):
        menu = UserMenu.objects.filter(roles__user__exact=request.user.id, parent=None).distinct().order_by("-order_priority", "id")
        ser = UserMenuSerializer(menu, many=True, read_only=True, context={"request": request}).data
        data = json.loads(json.dumps(ser))

        return Response(ser)
