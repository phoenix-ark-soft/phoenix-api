from django.contrib.auth import get_user_model
from rest_framework.viewsets import ReadOnlyModelViewSet

from ...serializers import UserSerializer
from core.constants import TAG_USER

User = get_user_model()


class UserViewSet(ReadOnlyModelViewSet):
    queryset = User.objects.filter(is_active=True)
    serializer_class = UserSerializer
    swagger_tags = [TAG_USER]
    search_fields = ["^username"]
    http_method_names = ["get"]

