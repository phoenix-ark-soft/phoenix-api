from django.contrib.auth import get_user_model
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from core.constants import TAG_USER

User = get_user_model()


class UserAvatarChange(APIView):
    permission_classes = [IsAuthenticated]
    swagger_tags = [TAG_USER]

    def patch(self, request: Request, *args, **kwargs):
        avatar = request.data.get("avatar")
        if avatar:
            request.user.avatar = avatar
            request.user.save()
            return Response({"avatar": request.user.avatar.url}, status=200)
        else:
            return Response("Поле avatar не было предоставлено", status=400)