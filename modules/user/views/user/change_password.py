from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from core.utils.ria import check_password_by_request
from core.constants import TAG_USER

User = get_user_model()


class UserPasswordChange(APIView):
    permission_classes = [IsAuthenticated]
    swagger_tags = [TAG_USER]

    def patch(self, request: Request, *args, **kwargs):
        if not check_password_by_request(request):
            return Response("Пароли не совпадают", status=403)
        password = request.data.get("password")
        if password:
            user = User.objects.get(id=request.user.id)
            user.password = make_password(password)
            user.save()
            return Response(status=200)
        else:
            return Response("Не был предоставлен новый пароль", status=400)