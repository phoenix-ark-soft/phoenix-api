from django.contrib.auth import get_user_model

import uuid

from django.conf import settings
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Group
from rest_framework import serializers

from core.email import send_email_template
from core.exception import ServerException
from .models import UserMenu, UserRoles

User = get_user_model()


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        exclude = ["permissions"]


class UserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True, read_only=True)
    avatar = serializers.SerializerMethodField("image_field")

    def image_field(self, obj):
        try:
            return obj.avatar.url
        except:
            return None

    class Meta:
        model = User
        fields = ["id", "username", "first_name", "last_name", "email", "avatar", "discord", "skype", "groups",
                  "phone", "patronymic"]


class UserRolesSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserRoles
        fields = "__all__"


class UserMenuSerializer(serializers.ModelSerializer):
    children = serializers.SerializerMethodField()

    class Meta:
        model = UserMenu
        fields = ["id", "title", "value", "icon", "href", "params", "children"]

    def get_children(self, obj):
        try:
            if obj is not None and obj.children is not None and obj.children.all() is not None:
                return UserMenuSerializer(
                    obj.children.filter(roles__user__exact=self.context["request"].user.id).distinct(),
                    many=True,
                    read_only=True,
                    context={"request": self.context["request"]}
                ).data
            else:
                return None
        except Exception as e:
            return None


class RegistrationSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        group = Group.objects.get(name="user")
        group.user_set.add(user)
        email_token = uuid.uuid4().hex
        user.token_email = email_token
        user.save()
        send_email_template([user.email],
                            "Подтверждение адреса электронной почты",
                            "email_confirm.html",
                            {
                                "link": f"{settings.BASE_PROTOCOL}://{settings.BASE_DOMAIN}{settings.BASE_PORT}/auth/confirm/?token={email_token}"})
        return user

    class Meta:
        model = User
        fields = ["username", "password", "email"]


class AuthorizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["username", "password"]


class ConfirmRegistrationSerializer(serializers.ModelSerializer):

    def update(self, instance, validated_data):
        instance.token_email = None
        instance.is_active = True
        instance.is_confirmed_email = True
        instance.save()
        return instance

    class Meta:
        model = User
        fields = ["token_email"]


class PasswordChangeSerializer(serializers.Serializer):
    email = serializers.CharField()

    def create(self, validated_data):
        raise ServerException()

    def update(self, instance, validated_data):
        token = uuid.uuid4().hex
        instance.token_password_change = token
        instance.save()
        send_email_template(
            [validated_data.get("email")],
            "Восстановление пароля",
            "password_forgot.html",
            {
                "link": f"{settings.BASE_PROTOCOL}://{settings.BASE_DOMAIN}{settings.BASE_PORT}/auth/recovery/?token={token}"
            }
        )
        return instance


class ConfirmPasswordChangeSerializer(serializers.Serializer):
    token = serializers.CharField()
    password = serializers.CharField()

    def create(self, validated_data):
        raise ServerException()

    def update(self, instance, validated_data):
        instance.token_password_change = None
        instance.password = make_password(validated_data.get("password"))
        instance.save()
        return instance


class AuthRespSerializer(serializers.Serializer):
    access = serializers.CharField(max_length=2048)
    user = UserSerializer()
