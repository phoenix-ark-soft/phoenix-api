from django.contrib import admin
from django.contrib.auth import get_user_model
from .models import UserMenu, UserRoles

User = get_user_model()


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ("id", "username", "is_superuser", "is_staff")
    ordering = ("-is_superuser", "id")


@admin.register(UserMenu)
class UserMenuAdmin(admin.ModelAdmin):
    list_display = ["title", "value", "icon", "href", "params", "parent"]
    ordering = ("id", )


@admin.register(UserRoles)
class UserRolesAdmin(admin.ModelAdmin):
    list_display = ["name", "description", "is_active"]
    ordering = ("id", )
