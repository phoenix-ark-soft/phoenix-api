from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class UserRoles(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=True, blank=True)
    user = models.ManyToManyField(User)

    class Meta:
        db_table = "pr_role"

    def __str__(self):
        return f"{self.name}"
