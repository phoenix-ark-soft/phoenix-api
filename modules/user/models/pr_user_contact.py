from django.db import models
from .pr_user import UserMainModel

class UserContact(models.Model):
    user = models.ManyToManyField(UserMainModel)
    contact_name = models.CharField(max_length=150, verbose_name="Наименование контакта")
    contact_url = models.CharField(max_length=1500, verbose_name="Адрес контакта")

    class Meta:
        db_table = "pr_user_contact"
