from django.db import models
from . import UserRoles


class UserMenu(models.Model):
    parent = models.ForeignKey("self", related_name="children", on_delete=models.CASCADE, null=True, blank=True)
    icon = models.CharField(max_length=256)
    title = models.CharField(max_length=80)
    value = models.CharField(max_length=80)
    href = models.TextField(null=True, blank=True)
    params = models.TextField(null=True, blank=True)
    order_priority = models.IntegerField(null=True, blank=True)
    roles = models.ManyToManyField(UserRoles, related_name="roles")

    class Meta:
        db_table = "pr_menu"

    def __str__(self):
        return f"{self.title} ({self.href})"
