from django.contrib.auth import get_user_model
from django.db import models
from modules.document_flow.models.doc_document import DocDocument

User = get_user_model()


class Commentaries(models.Model):
    text = models.TextField(verbose_name="Комментарий")
    entity = models.ForeignKey(DocDocument, on_delete=models.CASCADE, verbose_name="Сущность")
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, verbose_name="Автор")
    parent_id = models.IntegerField(null=True, blank=True, verbose_name="Комментарий-родитель")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Создан")

