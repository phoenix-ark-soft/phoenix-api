# Generated by Django 4.2.2 on 2023-09-17 06:49

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Commentaries',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(verbose_name='Комментарий')),
                ('parent_id', models.IntegerField(blank=True, null=True, verbose_name='Комментарий-родитель')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
            ],
        ),
    ]
