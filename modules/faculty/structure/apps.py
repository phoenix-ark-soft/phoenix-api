from django.apps import AppConfig


class FacultyStructureConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'modules.faculty.structure'
    verbose_name = "Структура факультета"
