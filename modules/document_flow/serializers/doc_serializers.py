from rest_framework import serializers
from ..models import DocDocument, DocType


class DocTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = DocType
        fields = "__all__"


class DocDocumentSerializer(serializers.ModelSerializer):
    doc_type = DocTypeSerializer(many=False, read_only=True)

    class Meta:
        model = DocDocument
        fields = "__all__"
