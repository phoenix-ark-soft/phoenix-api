from django.db import models


class DocType(models.Model):
    doc_type = models.CharField(max_length=200, primary_key=True, unique=True)
    description = models.CharField(max_length=512, verbose_name="Описание типа документа")
    can_accept_commentaries = models.BooleanField(default=True)
    can_accept_reactions = models.BooleanField(default=True)
    can_accept_tags = models.BooleanField
    for_admin_only = models.BooleanField(default=True)
    is_disabled = models.BooleanField(default=False)

    class Meta:
        db_table = "doc_type"
