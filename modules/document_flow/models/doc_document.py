from django.contrib.auth import get_user_model
from django.db import models
from .doc_type import DocType

User = get_user_model()


class DocDocument(models.Model):
    doc_type = models.ForeignKey(DocType, on_delete=models.SET_NULL, null=True, blank=True)
    data = models.JSONField()
    data_type = models.CharField(default="json")

    create_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    create_dt = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "doc_document"
