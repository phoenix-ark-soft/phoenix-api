from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import CreateAPIView
from ..models import DocDocument, DocType
from ..serializers import DocDocumentSerializer


class RegisterDocumentView(CreateAPIView):
    queryset = DocDocument.objects.all()
    serializer_class = DocDocumentSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        ...
