from django.apps import AppConfig


class DocumentFlowConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'modules.document_flow'
