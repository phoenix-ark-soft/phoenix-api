from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include("modules.tags.urls")),
    path('api/v1/', include("modules.user.urls")),
    path('api/v1/', include("modules.faculty.structure.urls")),
    path('api/v1/', include("modules.faculty.laboratories.urls")),
    path('api/v1/', include("modules.projects.ideas.urls")),
    path('api/v1/', include("modules.work.urls")),
    path('api/v1/', include("modules.questions.urls")),
    path('api/v1/', include("modules.courses.north_valley.urls")),
    path('api/v1/', include("modules.news.fac_news.urls")),
    path('api/v1/', include("modules.courses.fist_school.urls")),
    path('api/auth/', include('modules.user.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
