from rest_framework.request import Request


def get_client_ip(request: Request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def get_header(request: Request, name: str | list[str]) -> str | list[str]:
    """Получение заголовка или списка заголовков"""
    if isinstance(name, str):
        return request.headers.get(name)
    else:
        return [request.headers.get(n) for n in name]

